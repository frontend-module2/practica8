const selectElement = document.getElementById('cities');
let tabTemplates = {};

selectElement.addEventListener('change', (event) => {
	getInformation(event.target.value);
});

function getInformation(cityName) {
	showLoading();
	hideContent();
	getCityWeatherInformation(cityName);
}

function tabSelected(tabSelected) {
	changeTabStates(tabSelected);
	showSelectedTabContent(tabSelected);
}

function changeTabStates(tabSelected) {
	let tabs = document.querySelectorAll('.nav-link');
	tabs.forEach((tab, index) => {
		if (index == (tabSelected - 1)) {
			tab.classList.add('active');
		} else {
			tab.classList.remove('active');
		}
	});
}

function showSelectedTabContent(tabSelected) {
	let tabContents = document.querySelectorAll('.tab');
	tabContents.forEach((tabContent, index) => {
		if (index == (tabSelected - 1)) {
			fillTabContent(tabTemplates[tabSelected], tabContent);
			tabContent.classList.remove('tab--hidden');
		} else {
			tabContent.classList.add('tab--hidden');
		}
	});
}

function fillTabContent(content, element) {
	element.innerHTML = content;
}

function showLoading() {
	let loading = document.getElementById('loading');
	loading.classList.remove('main__loading--hidden');
}

function hideLoading() {
	let loading = document.getElementById('loading');
	loading.classList.add('main__loading--hidden');
}

function hideContent() {
	let loading = document.getElementById('weatherContent');
	loading.classList.add('main__content--hidden');
}

function showContent() {
	let loading = document.getElementById('weatherContent');
	loading.classList.remove('main__content--hidden');
}

function getCityWeatherInformation(cityName) {
	getCityWeather(cityName).then((response) => (
		response.json()
	)).then((information) => {
		tabTemplates = defineTabTemplates(information);
		hideLoading();
		showData(information);
		showContent();
		tabSelected('1');
	});
}

function defineTabTemplates(information) {
	return {
		1:`
			<div>Temperature:${information.main.temp}</div>
			<div>Humidity:${information.main.humidity}</div>
			<div>Feels like:${information.main.feels_like}</div>
			<div><span>${information.main.temp_min}</span> - <span>${information.main.temp_max}</span></div>
		`,
		2:`
			<div>Longitude:${information.coord.lon}</div>
			<div>Latitude:${information.coord.lat}</div>
		`,
		3:`
			<div>Speed:${information.wind.speed}</div>
			<div>Deg:${information.wind.deg}</div>
			<div>Gust:${information.wind.gust}</div>
		`,
	}
}

function showData(weatherData) {
	const icon = "http://openweathermap.org/img/wn/"+weatherData.weather[0].icon+"@2x.png"
    document.getElementById("weather").innerHTML =
		`
		<div class="image-section col-7">
			<div class="temperatura">
				<img src="${icon}">
				<h1>${weatherData.main.temp}</h1>
				<span>°C | °F</span>
				<h1>${farenheitToCelcius(weatherData.main.temp)}</h1>
			</div>
			<div class="information">
				<div><span class="fw-bold">Prob. de precipitaciones:</span> ${weatherData.main.pressure}hPa</div>
				<div><span class="fw-bold">Humedad:</span> ${weatherData.main.humidity}%</div>
				<div><span class="fw-bold">Viento:</span> ${weatherData.wind.speed}Km/h</div>
			</div>
		</div>
		<div class="information col">
			<h3 class="text-end">${weatherData.name}</h3>
			<div class="text-end">${getTime()}</div>
			<div class="text-end text-capitalize">${weatherData.weather[0].description}</div>
		</div>
		`
}

function getTime() {
	let actualDate = new Date().toLocaleDateString('es', { weekday:"long", day:"numeric"});
	let actualTime = new Date().toLocaleTimeString('en-US', 
	{
		hour: '2-digit',
		minute: '2-digit',
	})
	return actualDate +' '+ actualTime;
}

function farenheitToCelcius(temp) {
	return Math.round(((temp * (9/5)) + 32)*100) /100;
}

window.onload = () => {
	let selectELement = document.getElementById('cities');
	getInformation(selectELement.value);
}